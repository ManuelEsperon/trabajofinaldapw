@extends('plantillas.master')

@section('title')
Eventalia - Gestión de Eventos
@stop

@section('central')
<h2>Información sobre Eventalia</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend lacus quis ornare maximus. Aenean aliquam rutrum diam, non ultricies leo bibendum sit amet. Praesent eleifend, lorem sollicitudin mollis rutrum, erat sem imperdiet turpis, ut eleifend justo dui eu neque. Proin sed justo nec ex dapibus pulvinar. Morbi arcu justo, rhoncus ut metus non, condimentum condimentum tortor. Morbi porta elit a lacus maximus lobortis. Integer id iaculis leo. Vivamus eget ipsum odio. Donec nec urna et ante sodales luctus. Sed lacinia nulla eget fringilla posuere. Aliquam sagittis fringilla justo, a vestibulum tellus semper eget.</p>

<p>Mauris nec leo velit. Praesent tempus non dui id rutrum. Mauris vitae lectus a ligula pulvinar accumsan at eget augue. Vestibulum vulputate eu felis accumsan blandit. Sed auctor euismod odio nec lacinia. Nunc luctus sodales ipsum, ut vestibulum enim lobortis eget. Nullam aliquet id tortor et mollis. Praesent nec diam nisi. Pellentesque justo orci, volutpat id ipsum vitae, efficitur feugiat nulla.</p>
@stop