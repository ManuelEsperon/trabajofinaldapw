@extends('plantillas.master')

@section('title')
Edición de Eventos
@stop

@section('central')
<h2>EDICIÓN DE EVENTO</h2>
<form action="{{ route('eventos.update',$evento->id) }}" method="post">
    @method('PUT')
    @include('eventos._form')
    <input type="reset" name="Limpiar" />
    <input type="submit" class="btn btn-primary" value="Actualizar Evento" />
</form>
@stop