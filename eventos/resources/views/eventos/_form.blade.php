@csrf
<div class="mb-3">
    <label for="titulo" class="form-label">Título Evento:</label>
    <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo',$evento->titulo) }}" required maxlength="150">
    @error('titulo')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>

<div class="mb-3">
    <label for="telefono" class="form-label">Teléfono:</label>
    <input type="text" class="form-control" id="telefono" name="telefono" value="{{ old('telefono',$evento->telefono) }}" maxlength="15">
    @error('telefono')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="direccion" class="form-label">Dirección:</label>
    <input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion',$evento->direccion) }}" maxlength="15" required>
    @error('direccion')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="cartel" class="form-label">Fotografía cartel:</label>
    <input type="file" class="form-control" id="cartel" name="cartel" value="{{ old('cartel',$evento->cartel) }}" maxlength="100">
    @if ($errors->has('cartel'))
    <small class='alert alert-danger'>{{ $errors->first('cartel') }}</small>
    @endif
</div>
<div class="mb-3">
    <label for="latitud" class="form-label">Latitud:</label>
    <input type="text" class="form-control" id="latitud" name="latitud" value="{{ old('latitud',$evento->latitud) }}" maxlength="20">
    @error('latitud')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="longitud" class="form-label">Longitud:</label>
    <input type="text" class="form-control" id="longitud" name="longitud" value="{{ old('longitud',$evento->longitud) }}" maxlength="20">
    @error('longitud')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="fecha_inicio" class="form-label">Fecha Inicio:</label>
    <input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio" value="{{ old('fecha_inicio',$evento->fecha_inicio) }}">
    @error('fecha_inicio')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>
<div class="mb-3">
    <label for="fecha_fin" class="form-label">Fecha Fin:</label>
    <input type="date" class="form-control" id="fecha_fin" name="fecha_fin" value="{{ old('fecha_fin',$evento->fecha_fin) }}">
    @error('fecha_fin')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
</div>