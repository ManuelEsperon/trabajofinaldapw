@extends('plantillas.master')

@section('title')
Alta de Eventos
@stop

@section('central')
<h2>ALTA DE EVENTO</h2>
<form action="{{ route('eventos.store') }}" method="post" enctype="multipart/form-data">
    @include('eventos._form')
    <input type="reset" name="Limpiar" />
    <input type="submit" class="btn btn-primary" name="Alta de Evento" />
</form>
@stop