<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Entrada Eventalia</title>
</head>
<body>
    <center>
        SU ENTRADA EVENTALIA
        <h3>$evento->titulo</h3>
    </center>
    <table>
        <tr valing="top" align="center">
            <td>
                <img src="{{ URL::asset('storage/$evento->cartel'}}" width="300">
            </td>
            <td>
                <br/><br/>
                <img src="data:image/png;base64,{!! base64_encode(QrCode::format('svg')->size(150)->generate($entrada->codigoqrcontrol !!}">
                <h4>Muestre este código QR <br/>a la entrada del espectáculo</h4>
            </td>
        </tr>
    </table>
    <hr>
    Fecha de compra: {{ \Carbon\Carbon::parse($entrada->fecha_venta)->format('d/m/Y') }} || Fecha del evento: {{ \Carbon\Carbon::parse($entrada->fecha_evento)->format('d/m/Y') }} <br><br>
    Precio: {{ $entrada->precio }}<br />
    Asiento: {{ $entrada->asiento }}<br /> 
    <h3>Localización del evento</h3>
    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('svg')->size(150)->geo($evento->latitud, $evento->longitud)) !!} ">
</body>
</html>


Evento: {{ $evento->titulo }}<br />
Foto: <img src='{{ URL::asset("storage/$evento->cartel") }}' width="320" /><br />
Fecha Venta: {{ \Carbon\Carbon::parse($entrada->fecha_venta)->format('d/m/Y') }}<br />
Fecha del Evento: {{ \Carbon\Carbon::parse($entrada->fecha_evento)->format('d/m/Y') }}<br />
Precio: {{ $entrada->precio }}<br />
Asiento: {{ $entrada->asiento }}<br />
<hr />
Codigo QR validación entrada: {!! QrCode::size(150)->generate($entrada->codigoqrcontrol) !!} <br />
<hr />
Código QR de Google Maps: {!! QrCode::size(150)->geo($evento->latitud, $evento->longitud) !!}
