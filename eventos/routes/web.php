<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::resource('eventos', EventoController::class)->middleware('auth');

Route::get('/eventoscomprar', [EventoController::class, 'comprar'])->name('eventos.comprar')->middleware('auth');
Route::post('/eventoscomprar', [EventoController::class, 'comprarStore'])->name('eventos.comprarstore')->middleware('auth');
Route::get('/misentradas', [EventoController::class, 'misentradas'])->name('eventos.misentradas')->middleware('auth');
Route::get('/infoentrada/{entrada}', [EventoController::class, 'infoentrada'])->name('eventos.infoentrada')->middleware('auth');
