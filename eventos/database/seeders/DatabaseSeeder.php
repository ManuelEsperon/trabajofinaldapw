<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Evento;
use App\Models\EventoUser;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // \App\Models\User::factory(5)->create();
        \App\Models\User::factory(5)->create();
        Evento::factory(15)->create();
        EventoUser::factory(30)->create();
    }
}
