<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventoUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('evento_id');
            $table->date('fecha_venta')->comment('Fecha de venta de la entrada');
            $table->date('fecha_evento')->comment('Fecha del evento para esta entrada');
            $table->integer('precio')->comment('Precio de la entrada')->nullable();
            $table->string('asiento', 15)->comment('Número de asiento')->nullable();
            $table->string('codigoqrcontrol', 50)->comment('Codigo QR referente a la entrada.')->nullable();
            $table->timestamps();

            // Relaciones con las otras tablas:
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade')->onUpdate('cascade');
            
            // Ésto se podría poner también como:
            // $table->foreignId('user_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            // $table->foreignId('evento_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_user');
    }
}
