<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 150)->comment("Titulo del evento");
            $table->string('telefono', 15)->comment("Teléfono del evento")->nullable();
            $table->string('direccion', 150)->comment("Dirección del evento");
            $table->string('cartel', 100)->comment("Cartel/fotografía del evento.");
            $table->string('latitud', 20)->comment("Coordenadas geográficas: Latitud")->nullable();
            $table->string('longitud', 20)->comment("Coordenadas geográficas: Longitud")->nullable();
            $table->date('fecha_inicio')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->date('fecha_fin')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
