<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Evento;
use App\Models\EventoUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventoUserFactory extends Factory
{
    protected $model = EventoUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //  array('user_id', 'evento_id', 'fecha_venta', 'fecha_evento', 'precio', 'asiento', 'codigoqrcontrol')
        $totalUsuarios = User::all()->count();
        $totalEventos = Evento::all()->count();

        return [
            'user_id' => $this->faker->numberBetween(1, $totalUsuarios),
            'evento_id' => $this->faker->numberBetween(1, $totalEventos),
            'fecha_venta' => $this->faker->date(),
            'fecha_evento' => $this->faker->date(),
            'precio' => $this->faker->randomNumber(),
            'asiento' => $this->faker->buildingNumber(),
            'codigoqrcontrol' => $this->faker->ean13()
        ];
    }

    /* Control de fechas cos eventos xa creados
    public function definition()
        {
            $eventos = Evento::all();
            $eventoNum = $this->faker->numberBetween(1, $eventos->count());
            $users = User::all();
            $userNum = $this->faker->numberBetween(1, $users->count());
            $fecha_venta = $this->faker->dateTimeBetween(new DateTime($eventos[$eventoNum - 1]->fecha_inicio), new DateTime($eventos[$eventoNum - 1]->fecha_fin));
            return [
                "user_id" => $userNum,
                "evento_id" => $eventoNum,
                "fecha_venta" => $fecha_venta,
                "fecha_evento" => $this->faker->dateTimeBetween($fecha_venta, $eventos[$eventoNum - 1]->fecha_fin),
                "precio" => $this->faker->randomFloat(2, 0, 3000),
                "asiento" => $this->faker->randomNumber(4),
                "codigoqrcontrol" => $this->faker->ean13(),
            ];
        }
    */
}
