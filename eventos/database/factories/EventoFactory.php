<?php

namespace Database\Factories;

use DateInterval;
use App\Models\Evento;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventoFactory extends Factory
{
    protected $model = Evento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fecha_inicio = $this->faker->dateTimeBetween("+0 days", "+2 years");
        $fecha_final = clone $fecha_inicio;
        $fecha_final->add(new DateInterval("P1M"));
        return
            [
                'titulo' => $this->faker->company(),
                'telefono' => $this->faker->phonenumber(),
                'direccion' => $this->faker->address(),
                'cartel' => "cartel_por_defecto.jpg",
                'latitud' => $this->faker->latitude(),
                'longitud' => $this->faker->longitude(),
                'fecha_inicio' => $fecha_inicio, // $this->faker->date(),
                'fecha_fin' => $this->faker->dateTimeBetween($fecha_inicio, $fecha_final), // Este xa funciona ^^
                // 'fecha_fin' => $this->faker->date()
            ];
    }
}
