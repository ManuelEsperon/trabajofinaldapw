<?php

namespace App\Http\Controllers;

use PDF;
use auth;
use File;
use App\Models\Evento;
use App\Models\EventoUser;
use App\Mail\enviarEntrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreEventoRequest;
use Illuminate\Notifications\Notification;
use App\Http\Requests\StoreEventoUserRequest;

class EventoController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth')->except('create');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Conseguir todos los eventos.
        $eventos = Evento::paginate(5);
        return view('eventos.index')->with('eventos', $eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventos.create')->with('evento', new Evento());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventoRequest $request)
    {
        $datosvalidados = $request->validated();
        $datosvalidados['cartel'] = 'cartel_defecto.jpg';

        // Almacenamos el archivo recibido en el servidor
        if ($request->file('cartel')) {
            $fichero = $request->file('cartel');
            $nuevonombre = time() . '_' . $fichero->getClientOriginalName();

            // Movemos el archivo al destino. public/carteles/xxxxxx.jpg
            //$fichero->move('carteles', $nuevonombre);

            // Filesystem como 'local'. Esta opcion almacena en /storage/app/carteles/nombre_al_azar.
            //$ruta = $request->cartel->store('carteles');

            // Filesystem como 'local'. Esta opcion almacena en /storage/app/public/carteles/nombre_al_azar.
            $ruta = $request->cartel->store('carteles', 'public');
            $datosvalidados['cartel'] = $ruta;
        }

        // Almacenamos en la base de datos
        Evento::create($datosvalidados);

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento dado de alta correctamente.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('eventos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Evento $evento)
    {
        // Conseguimos la informacion del evento
        return view('eventos.cartel')->with('evento', $evento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento $evento)
    {
        return view('eventos.edit')->with('evento', $evento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEventoRequest $request, Evento $evento)
    {
        // Actualizamos el evento.
        $evento->update($request->validated());

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento actualizado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('eventos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento $evento)
    {
        // Borramos el registro.
        $evento->delete();

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Evento borrado correctamente.');
        Session::flash('alert-class', 'alert-success');

        // Redireccionamos al index.
        return redirect()->route('eventos.index');
    }

    // Devuelve el formulario para comprar una entrada.
    public function comprar()
    {
        // Conseguimos la lista de eventos disponibles.
        $eventos = Evento::all();
        return view('eventos.comprar')->with('eventos', $eventos)->with('entrada', new EventoUser());
    }

    public function comprarStore(StoreEventoUserRequest $request)
    {
        // Datos validados
        $datos_validados = $request->validated();

        // El id del usuario  // auth()->user()
        $datos_validados['user_id'] = auth()->user()->id;

        // Generamos el codigo qrcontrol.
        // Hacemos un md5 de fecha actual del sistema + id usuario + id evento
        $datos_validados['codigoqrcontrol'] = md5(microtime() . $datos_validados['user_id'] . $datos_validados['evento_id']);

        // La fecha de venta sería la fecha del sistema.
        $datos_validados['fecha_venta'] = date("Y-m-d");

        // Almacenamos en la tabla y redireccionamos al index.
        $entrada = EventoUser::create($datos_validados);

        // Buscamos la información del evento para pasarla al pdf
        $evento = Evento::where('id','=',$datos_validados['evento_id'])->first();
        // Generamos el PDF.
        $datos = [
            'evento' => $evento,
            'entrada' => $entrada
        ];
        
        // Para descargar el PDF
        // $pdf = PDF::loadView('eventos.entradapdf', $datos);
        // return $pdf->download('entrada.pdf');

        // Para enviar por correo electrónico
        // Guardo el PDF que se genera para adjuntarlo al correo
        $rutaFicheronEntradaPDF = 'storage/Eventalia_'.md5(time()).'.pdf';
        // Genero el PDF y lo guardo en la ruta con ese nombre
        $pdf = PDF::loadView('eventos.entradapdf', $datos);
        $pdf->save($rutaFicheronEntradaPDF);
        // Enviamos por correo electrónico
        try {
            Mail::to(auth()->user()->email)->send(new enviarEntrada($rutaFicheronEntradaPDF));
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        // Borramos el fichero temporal de la entrada PDF
        File::delete(public_path($rutaFicheronEntradaPDF));

        // Guarde un mensaje Flash.
        Session::flash('mensaje', 'Venta realizada correctamente. Consulte su buzón de correo.');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('eventos.index');
    }

    public function misentradas()
    {
        // Listado con mis entradas.
        // Obtenemos las entradas del usuario que está conectado auth()->user()->id;
        $entradas = EventoUser::where('user_id', auth()->user()->id)->orderBy('fecha_evento')->get();
        return view('eventos.misentradas')->with('entradas', $entradas);
    }

    public function infoentrada(EventoUser $entrada)
    {
        // Averiguamos el titulo del evento para mostrarlo en el detalle de la entrada.
        $evento = Evento::where("id", "=", $entrada->evento_id)->first();

        // Listado con mis entradas.
        // Obtenemos las entradas del usuario que está conectado auth()->user()->id;
        return view('eventos.entrada')->with('entrada', $entrada)->with('evento', $evento);
    }
}
