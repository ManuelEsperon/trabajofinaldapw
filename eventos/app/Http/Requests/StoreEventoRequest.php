<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|max:150',
            'telefono' => 'nullable|max:15',
            'direccion' => 'required|max:150',
            'cartel' => 'required|mimes:png,jpg,jpeg|max:2048',
            'latitud' => 'nullable|max:20',
            'longitud' => 'nullable|max:20',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date'
        ];
    }
}
